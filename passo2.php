<?php

use Utilitarios\Ordenar;
use Utilitarios\Database\MySQL;

require 'autoload.php';

$lista_de_compras = Ordenar::listaDeCompras(require 'lista-de-compras.php');

MySQL::createSchema('script.sql');

MySQL::migration($lista_de_compras);
