<?php

use Utilitarios\Ordenar;
use Utilitarios\Csv\CriarArquivo;

require 'autoload.php';

$lista_de_compras = Ordenar::listaDeCompras(require 'lista-de-compras.php');

$csv = new CriarArquivo('compras-do-ano.csv');

$csv->escrever($lista_de_compras);
